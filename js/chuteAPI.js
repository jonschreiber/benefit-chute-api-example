var oauth_token='d2c6a00b63b7b697df23c909c4c7d37b29ddb8ceea4d89b5aa6576fde7ffa612';

function fetchAlbums(page) {
  $.ajax({
    url: 'http://api.getchute.com/v2/albums',
    data: {
      oauth_token: oauth_token,
      per_page: 100,
      page: page
    },
    type: 'GET',
    dataType: 'json'
  }).done(function (res) {
    var albums = res.data,
      albumCount = albums.length;

    for (var i = 0; i < albumCount; i++) {
      if (!page) page = 1;
      $('#albums').append(
          '<div class="album" data-id="' + albums[i].shortcut + '"> \
            <table> \
              <tr> \
                <td>Album:</td> \
                <td>' + albums[i].name + '</td> \
              </tr> \
              <tr> \
                <td>Images:</td> \
                <td>' + albums[i].images_count + '</td> \
              </tr> \
              <tr> \
                <td>Updated:</td> \
                <td>' + albums[i].updated_at + '</td> \
              </tr> \
            </table> \
          </div>'
        );
    }

    if (res.pagination.next_page) {
      fetchAlbums(page + 1);
    } else {
      $('.album').on('click', function() {
        var per_page = 5;
        $('#nav').empty();
        prefetchAssets($(this).data('id'), per_page).done(function (assets, hasMore) {
          assets.forEach(function (list, index) {
            var $link = $('<a href="#"> ' + (index + 1) + ' </a>');
            $link.click(function () {
              updateAssets(assets[index]);
            });
            $('#nav').append($link);
          });

          if (hasMore) {
            var $moreEl = $('<a href="#">...</a>');
            $moreEl.click(function() {
            });

            $('#nav').append('...');
          }
        });
      });
    }
  });
}

function updateAssets(assets) {

  // $.ajax({
  //   url: url,
  //   data: {},
  //   type: 'GET',
  //   dataType: 'json'
  // }).done(function (res) {
    $('#assets').empty();
    $('footer:not(#nav)').empty();

    var assets = assets,
      assetCount = assets.length;

    for (var i = 0; i < assetCount; i++) {
      $('#assets').append(
          '<div class="asset" data-id="' + assets[i].shortcut + '" data-chute-asset="' + assets[i].chute_asset_id + '"> \
            <div><a href="' + assets[i].url + '"><img src="' + assets[i].thumbnail + '" /></a></div> \
            <table> \
              <tr> \
                <td>User:</td> \
                <td>' + assets[i].user.name + '</td> \
              </tr> \
              <tr> \
                <td>Votes:</td> \
                <td>' + assets[i].votes + '</td> \
              </tr> \
              <tr> \
                <td>Hearts:</td> \
                <td>' + assets[i].hearts + '</td> \
              </tr> \
              <tr> \
                <td>Updated:</td> \
                <td>' + assets[i].updated_at + '</td> \
              </tr> \
            </table> \
          </div>'
        );
    }

    // if (res.pagination.previous_page) {
    //   $('footer').append('<button class="prev">Prev</button>');
    //   $('.prev').on('click', function() {
    //     fetchAssets(album_id, res.pagination.previous_page);
    //   });
    // }

    // if (res.pagination.next_page) {
    //   $('footer').append('<button class="next">Next</button>');
    //   $('.next').on('click', function() {
    //     fetchAssets(album_id, res.pagination.next_page);
    //   });
    // }
  // });
}

function prefetchAssets(album_id, per_page) {
  var url = 'http://api.getchute.com/v2/albums/' + album_id + '/assets';
  var promise = $.Deferred();
  var assets = [];

  $.ajax({
    url: url,
    data: {
      per_page: 100,
      oauth_token: oauth_token
    },
    type: 'GET',
    dataType: 'json'
  }).done(function (res) {
    url = res.pagination.next_page;
    var assetCache = res.data.slice();

    while (assetCache.length) {
      assets.push(assetCache.slice(0, per_page));
      assetCache = assetCache.slice(per_page);
    }

    if (url) {
      promise.resolve(assets, true);
    } else {
      promise.resolve(assets, false);
    }
  });

  return promise;
}